# Node XML Sitemap

Node XML Sitemap module intends to provide XML URL for each node based on content type.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/node_xml_sitemap).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/node_xml_sitemap).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following modules:

- [XML Sitemap](https://www.drupal.org/project/xmlsitemap)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/
installing-drupal-modules).

## Configuration

Ther are no configuration requied. This page will give you list valid URLs,
for yous project (/admin/node-sitemap-listing).

## Maintainers

Current maintainers:

- [Prabu (PrabuEla)](https://www.drupal.org/u/prabuela)

Supporting organizations:

- [Specbee](https://www.drupal.org/specbee) Sponsored the module development
